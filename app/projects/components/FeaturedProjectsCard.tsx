"use client";
import { GithubIcon } from "@/app/components/Icons";
import Image, { StaticImageData } from "next/image";
import Link from "next/link";
import React from "react";
import { motion } from "framer-motion";

const FramerImage = motion(Image);

type Props = {
  type: string;
  title: string;
  summary: string;
  image: StaticImageData;
  link?: string;
  gitLink?: string;
};

function FeaturedProjectsCard({
  type,
  title,
  summary,
  image,
  link,
  gitLink,
}: Props) {
  return (
    <article
      className="w-full relative flex items-center justify-center rounded-3xl border border-solid 
    border-dark dark:border-light bg-light dark:bg-dark shadow-2xl p-12 rounded-br-2xl lg:flex-col
    lg:p-8 xs:rounded-2xl xs:rounded-br-3xl xs:p4"
    >
      <div
        className="absolute top-0 -right-3 -z-10 w-[101%] h-[103%] rounded-[2.5rem] bg-dark 
      dark:bg-light rounded-br-3xl xs:-right-2 sm:h-[102%] sm:w-full xs:rounded-[1.5rem]"
      ></div>
      <Link
        href={link ? link : ""}
        target="_blank"
        className="w-1/2 cursor-pointer overflow-hidden rounded-lg lg:w-full"
      >
        <FramerImage
          whileHover={{ scale: 1.05 }}
          transition={{ duration: 0.2 }}
          src={image}
          alt={title}
          className="w-full h-auto"
          priority={true}
          sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 50vw"
        />
      </Link>
      <div className="w-1/2 flex flex-col items-start justify-between pl-6 lg:w-full lg:pl-0 lg:pt-6">
        <span className="text-primary dark:text-primaryDark font-medium text-xl xs:text-base">
          {type}
        </span>
        <Link
          href={link ? link : ""}
          target="_blank"
          className="hover:underline underline-offset-2"
        >
          <h2 className="my-2 w-full text-left text-4xl font-bold dark:text-light sm:text-sm">
            {title}
          </h2>
        </Link>
        <p className="my-2 font-medium text-dark dark:text-light sm:text-sm">
          {summary}
        </p>
        <div className="mt-2 flex items-center">
          {gitLink && (
            <Link
              href={gitLink ? gitLink : ""}
              target="_blank"
              className="w-10"
            >
              <GithubIcon className="dark:text-light" />
            </Link>
          )}
          {link && (
            <Link
              href={link}
              target="_blank"
              className="ml-4 rounded-lg bg-dark dark:bg-light text-light dark:text-dark p-2 px-6 
              text-lg font-semibold sm:px-4 sm:text-base"
            >
              Visit project
            </Link>
          )}
        </div>
      </div>
    </article>
  );
}

export default FeaturedProjectsCard;
