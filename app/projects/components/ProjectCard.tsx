"use client";
import { GithubIcon } from "@/app/components/Icons";
import Image, { StaticImageData } from "next/image";
import Link from "next/link";
import React from "react";
import { motion } from "framer-motion";

const FramerImage = motion(Image);

type Props = {
  type: string;
  title: string;
  summary: string;
  image: StaticImageData;
  link?: string;
  gitLink?: string;
};

function ProjectCard({ type, title, summary, image, link, gitLink }: Props) {
  return (
    <article
      className="w-full flex flex-col items-center justify-center rounded-2xl border 
    border-solid border-dark dark:border-light bg-light dark:bg-dark p-6 relative"
    >
      <div
        className="absolute top-0 -right-3 -z-10 w-[101%] h-[103%] rounded-[2rem] bg-dark 
      dark:bg-light rounded-br-3xl md:-right-2 md:w-[101%] xs:h-[102%] xs:rounded-[1.5rem]"
      ></div>
      <Link
        href={link ? link : ""}
        target="_blank"
        className="w-full cursor-pointer overflow-hidden rounded-lg"
      >
        <FramerImage
          whileHover={{ scale: 1.05 }}
          transition={{ duration: 0.2 }}
          src={image}
          alt={title}
          className="w-full h-auto"
          priority={true}
          sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 50vw"
        />
      </Link>
      <div className="w-full flex flex-col items-start justify-between mt-4">
        <span className="text-primary dark:text-primaryDark font-medium text-xl lg:text-lg md:text-base">
          {type}
        </span>
        <Link
          href={link ? link : ""}
          target="_blank"
          className="hover:underline underline-offset-2"
        >
          <h2 className="my-2 w-full text-left text-3xl font-bold dark:text-light lg:text-2xl">
            {title}
          </h2>
        </Link>
        <div className="mt-2 w-full flex items-center justify-between">
          {link && (
            <Link
              href={link}
              target="_blank"
              className="text-lg font-semibold underline underline-offset-1 dark:text-light md:text-base"
            >
              Visit
            </Link>
          )}
          {gitLink && (
            <Link
              href={gitLink ? gitLink : ""}
              target="_blank"
              className="w-8 md:w-6"
            >
              <GithubIcon />
            </Link>
          )}
        </div>
      </div>
    </article>
  );
}

export default ProjectCard;
