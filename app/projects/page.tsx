import React from "react";
import CustomLayout from "../components/CustomLayout";
import AnimatedText from "../components/AnimatedText";
import FeaturedProjectsCard from "./components/FeaturedProjectsCard";
import chatAppIllu from "../../public/images/projects/chat-saas-app-image-illu.png";
import NamiCodeIllu from "../../public/images/projects/NamiCode-home-page.jpg";
import DropBoxCloneIllu from "../../public/images/projects/DropBox-Clone-illu.png";
import PayloadEcommerceIllu from "../../public/images/projects/PayloadCMS-illu.png";
import ProjectCard from "./components/ProjectCard";
import TransitionEffect from "../components/TransitionEffect";

function page() {
  return (
    <>
      <TransitionEffect />
      <main className="w-full mb-16 flex flex-col items-center justify-center">
        <CustomLayout className="pt-16">
          <AnimatedText
            text="Imagination Trumps Knowledge!"
            className="!text-8xl mb-16 lg:!text-7xl sm:mb-8 sm:!text-6xl xs:!text-4xl"
          />

          <div className="grid grid-cols-12 gap-24 gap-y-32 xl:gap-x-16 lg:gap-x-8 md:gap-y-24 sm:gap-x-0">
            <div className="col-span-12">
              <FeaturedProjectsCard
                title="chat with everyone using AI"
                type="portfolio"
                summary="Developed a SaaS app enabling users to chat globally in their native languages, powered by AI translation. Key technologies: TypeScript, NextJS, ReactJS, NodeJS, TailwindCSS, Firebase, Stripe. Highlights include subscription-based features and a secure middleware aligned with Firebase for route access control."
                image={chatAppIllu}
                gitLink="https://gitlab.com/MrAlpha/chat-saas-app"
                link="https://saas-chat-ai-app.vercel.app/"
              />
            </div>
            <div className="col-span-6 sm:col-span-12">
              <ProjectCard
                title="NamiCode"
                type="professionnal"
                image={NamiCodeIllu}
                summary="This Website was made in order to start my freelance journey with a friend as a web developper company. I've made it with Hostinger web creation tool because I wanted to test how noCode tool work and because it was faster. You can find any information about us and our service and even get in touch to start your project."
                link="https://namicode.net/"
              />
            </div>
            <div className="col-span-6 sm:col-span-12">
              <ProjectCard
                image={DropBoxCloneIllu}
                title="DropBox Clone"
                type="portfolio"
                summary="A basic clone with dropbox where you can upload any kind of file and download the later on another device. You can only download your file and cannot share with other. This project was made in order to learn how to interact with file and upload them using drag and drop and firebase."
                link="https://dropbox-clone-portfolio-project.vercel.app/"
              />
            </div>
            <div className="col-span-12">
              <FeaturedProjectsCard
                title="Payload CMS Ecommerce website"
                type="portfolio"
                image={PayloadEcommerceIllu}
                summary="Crafted a fully responsive e-commerce site featuring authentication and Stripe payment integration. Utilized NextJS and MongoDB, incorporating Payload CMS for its customizable TypeScript-based templates. The site boasts a fully adaptable admin dashboard, enabling complete content and product management, user oversight, and order tracking."
                gitLink="https://github.com/MrAlphajet-Efrei/e-commerce"
              />
            </div>
          </div>
        </CustomLayout>
      </main>
    </>
  );
}

export default page;
