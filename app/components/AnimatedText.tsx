"use client";
import React from "react";
import { motion } from "framer-motion";

type Props = {
  text: string;
  className?: string;
};

const quote = {
  initial: {
    opacity: 1,
  },
  animate: {
    opacity: 1,
    transition: {
      delay: 0.5,
      staggerChildren: 0.08,
    },
  },
};

const singleCarac = {
  initial: {
    opacity: 0,
    y: 50,
  },
  animate: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 1,
    },
  },
};

function AnimatedText({ text, className }: Props) {
  return (
    <div className="w-full mx-auto py-2 flex items-center justify-center text-center overflow-hidde sm:py-8">
      <motion.h1
        className={`inline-block w-full text-dark dark:text-light font-bold capitalize text-6xl ${className}`}
        variants={quote}
        initial="initial"
        animate="animate"
      >
        {text.split(" ").map((carac, index) => (
          <motion.span
            key={carac + " - " + index}
            className="inline-block"
            variants={singleCarac}
          >
            {carac}&nbsp;
          </motion.span>
        ))}
      </motion.h1>
    </div>
  );
}

export default AnimatedText;
