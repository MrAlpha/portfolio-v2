import React from "react";
import CustomLayout from "./CustomLayout";
import Link from "next/link";

function Footer() {
  return (
    <footer
      className="w-full border-t-2 border-solid border-dark dark:border-light 
    font-medium text-lg dark:text-light sm:text-base"
    >
      <CustomLayout className="py-8 flex items-center justify-between lg:flex-col lg:py-6">
        <span>{new Date().getFullYear()} &copy; All Rights Reserved. </span>
        <div className="flex items-center lg:py-2">
          Build with{" "}
          <span className="text-primary dark:text-primaryDark text-2xl px-1">
            &#9825;
          </span>{" "}
          by&nbsp;
          <Link
            href="https://namicode.net/en/services"
            className="underline underline-offset-2"
            target="_blank"
          >
            yannick
          </Link>
        </div>
        <Link href="/" className="underline underline-offset-2">
          Say hello
        </Link>
      </CustomLayout>
    </footer>
  );
}

export default Footer;
