"use client";
import React from "react";
import { motion } from "framer-motion";
import Link from "next/link";

function Logo() {
  return (
    <motion.div
      className=" mt-2 cursor-pointer w-16 h-16 bg-dark text-light border border-solid border-transparent dark:border-light flex items-center justify-center rounded-full text-2xl font-bold"
      whileHover={{
        backgroundColor: [
          "#121212",
          "rgba(131,58,180,1)",
          "rgba(253,29,29,1)",
          "rgba(252,176,69,1)",
          "rgba(131,58,180,1)",
          "#121212",
        ],
        transition: { duration: 1, repeat: Infinity },
      }}
    >
      <Link href="/" className="">
        YY
      </Link>
    </motion.div>
  );
}

export default Logo;
