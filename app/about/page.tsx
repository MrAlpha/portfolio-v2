"use client";
import React, { useEffect, useRef } from "react";
import AnimatedText from "../components/AnimatedText";
import CustomLayout from "../components/CustomLayout";
import Image from "next/image";
import profilPic from "../../public/images/profile/developer-pic-3.jpg";
import { useInView, useMotionValue, useSpring } from "framer-motion";
import Skills from "./components/Skills";
import Experiences from "./components/Experiences";
import Education from "./components/Education";
import TransitionEffect from "../components/TransitionEffect";

const AnimatedNumber = ({ value }: { value: number }) => {
  const ref = useRef(null);

  const motionValue = useMotionValue(0);
  const springValue = useSpring(motionValue, { duration: 3000 });
  const isInView = useInView(ref, { once: true });

  useEffect(() => {
    if (isInView) {
      motionValue.set(value);
    }
  }, [isInView, motionValue, value]);

  useEffect(() => {
    springValue.on("change", (latest: any) => {
      if (ref.current && latest.toFixed(0) <= value) {
        // @ts-ignore
        ref.current.textContent = latest.toFixed(0);
      }
    });
  }, [springValue, value]);

  return <span ref={ref}></span>;
};

const StatDiv = ({ value, text }: { value: number; text: string }) => {
  return (
    <div className="flex flex-col items-end justify-center xl:items-center">
      <span className="inline-block text-7xl font-bold md:text-6xl sm:text-5xl xs:text-4xl">
        <AnimatedNumber value={value} />+
      </span>
      <h2
        className="text-xl font-medium capitalize text-dark/75 dark:text-light/75 xl:text-center
      md:text-lg sm:text-base xs:text-sm"
      >
        {text}
      </h2>
    </div>
  );
};

function page() {
  return (
    <>
      <TransitionEffect />
      <main className="flex w-full flex-col items-center justify-center dark:text-light">
        <CustomLayout className="pt-16">
          <AnimatedText
            text="Passion Fuels Purpose! "
            className="mb-16 !text-8xl lg:!text-7xl sm:!text-6xl xs:!text-4xl sm:mb-8"
          />
          <div className="grid w-full grid-cols-8 gap-16 sm:gap-8">
            <div className="col-span-3 flex flex-col items-start justify-start xl:col-span-4  md:order-2 md:col-span-8">
              <h2
                className="mb-4 text-lg font-bold uppercase text-dark/75 dark:text-light/75
            "
              >
                About Me
              </h2>

              <p className="font-medium">
                Hello there! I'm Yannick, a bit of a digital craftsman, one
                who's been weaving the web and cloud technologies into
                functional art for over a year now. As a freelance software
                engineer and web developer, my days are filled with coding,
                creating, and, yes, a bit of daydreaming about the endless
                possibilities that technology can bring to life. While ReactJS,
                NextJS, and NodeJS are my go-to tools, my experience spans a
                diverse array of technologies, allowing me to tailor each
                project with just the right technical ingredients.
              </p>
              <p className="font-medium my-4">
                Each project I take on is more than just a job; it's a journey.
                A journey where I get to dive deep into the dreams and visions
                of my clients, transforming them into tailor-made digital
                realities. Whether it's for a burgeoning startup or a titan of
                the banking world, I've navigated diverse terrains, always
                emerging with a solution that's not just effective, but also
                uniquely theirs. From concept to deployment, every stage is an
                adventure in problem-solving and innovation.
              </p>
              <p className="font-medium">
                What drives me? The thrill of a challenge. There's nothing quite
                like taking on a project that pushes my limits, compelling me to
                blend creativity with technical prowess. It's in these moments
                that I find the opportunity to grow, both as a developer and as
                a storyteller of sorts, where each line of code adds to the
                narrative of my client's aspirations.
              </p>
              <p className="font-medium">
                So, if you're looking to bring your digital dreams to life, why
                not embark on this creative journey with me? Let's build
                something extraordinary together, a web experience that not only
                meets your needs but also showcases your vision to the world.
                After all, your ambition deserves a digital stage, and I'm here
                to help you build it.
              </p>
            </div>
            <div
              className="col-span-3 xl:col-span-4 relative h-max rounded-2xl border-2 
          border-solid border-dark dark:border-light bg-light dark:bg-dark p-8 md:order-1
          md:col-span-8"
            >
              <Image
                src={profilPic}
                alt="yannick yanat"
                className="w-full h-auto rounded-2xl"
                priority={true}
                sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
              />
              <div className="absolute top-0 -right-3 -z-10 w-[102%] h-[103%] rounded-3xl bg-dark dark:bg-light"></div>
            </div>
            <div
              className="col-span-2 xl:col-span-8 flex flex-col xl:flex-row items-end 
          xl:items-center justify-between md:order-3"
            >
              <StatDiv value={2} text="satisfied clients" />
              <StatDiv value={5} text="projects completed" />
              <StatDiv value={1} text="years of experience" />
            </div>
          </div>
          <Skills />
          <Experiences />
          <Education />
        </CustomLayout>
      </main>
    </>
  );
}

export default page;
