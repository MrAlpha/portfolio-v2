"use client";
import React, { useRef } from "react";
import { motion, useScroll } from "framer-motion";
import LiIcon from "./LiIcon";

type DetailsProps = {
  position: string;
  company: string;
  companyLink?: string;
  time: string;
  address: string;
  work: string;
};

const Details = ({
  position,
  company,
  companyLink,
  time,
  address,
  work,
}: DetailsProps) => {
  const ref = useRef(null);
  return (
    <li
      ref={ref}
      className="my-8 first:mt-0 last:mb-0 w-[60%] mx-auto flex flex-col items-center justify-between md:w-[80%]"
    >
      <LiIcon reference={ref} />
      <motion.div
        initial={{ y: 50 }}
        whileInView={{ y: 0 }}
        transition={{ duration: 0.5, type: "spring" }}
      >
        <h3 className="capitalize font-bold text-2xl sm:text-xl xs:text-lg">
          {position}&nbsp;
          <a
            href={companyLink ? companyLink : ""}
            target="_blank"
            className="text-primary dark:text-primaryDark capitalize"
          >
            @{company}
          </a>
        </h3>
        <span className="capitalize font-medium text-dark/75 dark:text-light/75 xs:text-sm">
          {time} | {address}
        </span>
        <p className="font-medium w-full xs:text-sm">{work}</p>
      </motion.div>
    </li>
  );
};

function Experiences() {
  const ref = useRef(null);
  const { scrollYProgress } = useScroll({
    target: ref,
    offset: ["start end", "center start"],
  });
  return (
    <div className="my-64">
      <h2 className="font-bold text-8xl mb-32 w-full text-center md:text-6xl xs:text-4xl md:mb-16">
        Experiences
      </h2>

      <div className="w-[75%] mx-auto relative lg:w-[90%] md:w-full" ref={ref}>
        <motion.div
          style={{ scaleY: scrollYProgress }}
          className="absolute left-9 top-0 w-[4px] h-full bg-dark dark:bg-light origin-top 
          md:w-[2px] md:left-[30px] xs:left-[20px]"
        ></motion.div>
        <ul className="w-full flex flex-col items-start justify-between ml-4 xs:ml-2">
          <Details
            company="BNP Paribas Asset Management"
            companyLink="https://www.bnpparibas-am.com/"
            address="8 rue du port, Nanterre"
            position="DevOps Engineer"
            time="April 2023 - September 2023"
            work="In the BNP cloud migration project, I played a key role in enhancing our tech landscape. My primary missions involved developing CI/CD templates, crafting secure Docker images, and deploying sophisticated monitoring tools. This experience not only honed my skills in cloud technologies and security but also deepened my expertise in continuous integration and deployment."
          />
          <Details
            company="Société Générale"
            companyLink="https://particuliers.sg.fr/"
            address="2 Bd Franck Kupka, Puteaux"
            position="Full-stack Developper"
            time="December 2021 - April 2022"
            work="At Société Générale, I embraced a multifaceted role, navigating through various aspects of application development. From front-end design to cloud solutions, and from DevOps strategies to managing PostgreSQL databases, my journey was diverse. My expertise extended to backend development, utilizing NodeJS and .NET, where I was instrumental in creating, maintaining, and enhancing applications in the complex and dynamic environment of the banking sector."
          />
          <Details
            company="UHDP"
            companyLink=""
            address="remote"
            position="freelance - Full-stack developper"
            time="June 2022 - April 2023"
            work="My tenure at UHDP was a deep dive into the world of freelance client expectations, focusing on delivering top-quality websites that perfectly aligned with their guidelines, especially in terms of design mockups. This role was a significant growth opportunity for me as a developer, where I not only enhanced my full-stack development capabilities but also specialized in front-end tasks. Here, I concentrated on crafting bespoke websites with a keen eye on performance optimization and SEO, ensuring that each site wasn't just visually appealing, but also high-performing and search-engine friendly."
          />
        </ul>
      </div>
    </div>
  );
}

export default Experiences;
