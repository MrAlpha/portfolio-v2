import React from "react";
import { motion } from "framer-motion";

const Skill = ({ name, x, y }: { name: string; x: string; y: string }) => {
  return (
    <motion.div
      whileHover={{ scale: 1.1 }}
      initial={{ x: 0, y: 0 }}
      whileInView={{ x: x, y: y }}
      transition={{ duration: 1.3 }}
      viewport={{ once: true }}
      className="flex items-center justify-center rounded-full cursor-pointer font-semibold 
      bg-dark dark:bg-light text-light dark:text-dark py-3 px-6 shadow-dark dark:shadow-light 
      absolute lg:py-2 lg:px-4 md:text-sm md:py-1.5 md:px-3 xs:bg-transparent xs:dark:bg-transparent 
      xs:text-dark xs:dark:text-light xs:font-bold"
    >
      {name}
    </motion.div>
  );
};

type Props = {};

function Skills({}: Props) {
  return (
    <>
      <h2 className="font-bold text-8xl mt-64 w-full text-center md:text-6xl xs:text-4xl">
        Skills
      </h2>
      <div
        className="w-full h-screen relative flex items-center justify-center rounded-full bg-circularLight 
      dark:bg-darkCircularLight lg:h-[80vh] sm:h-[60vh] xs:h-[50vh] 
      lg:bg-circularLightLg lg:dark:bg-darkCircularLightlG
      md:bg-circularLightMd md:dark:bg-circularDarkMd
      sm:bg-circularLightSm sm:dark:bg-circularDarkSm"
      >
        <motion.div
          whileHover={{ scale: 1.5 }}
          className="flex items-center justify-center rounded-full cursor-pointer font-semibold bg-dark 
          dark:bg-light text-light dark:text-dark p-8 shadow-dark dark:shadow-light
          lg:p-6 md:p-4 xs:text-xs xs:p-2"
        >
          Web
        </motion.div>
        <Skill name="CSS" x="-5vw" y="-10vw" />
        <Skill name="HTML" x="-20vw" y="2vw" />
        <Skill name="Javascript" x="20vw" y="0vw" />
        <Skill name="Typescript" x="25vw" y="10vw" />
        <Skill name="ReactJS" x="0vw" y="12vw" />
        <Skill name="NextJS" x="-20vw" y="-15vw" />
        <Skill name="NodeJS" x="15vw" y="-12vw" />
        <Skill name="Postgresql" x="32vw" y="-5vw" />
        <Skill name="Sanity" x="0vw" y="-20vw" />
        <Skill name="Firebase" x="-25vw" y="18vw" />
        <Skill name="Docker" x="18vw" y="18vw" />
        <Skill name="Tailwind CSS" x="-25vw" y="-6vw" />
        <Skill name="DevOps" x="-18vw" y="10vw" />
        <Skill name="Kubernetes" x="20vw" y="-20vw" />
      </div>
    </>
  );
}

export default Skills;
