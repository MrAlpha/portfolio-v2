"use client";
import React, { useRef } from "react";
import { motion, useScroll } from "framer-motion";
import LiIcon from "./LiIcon";

type DetailsProps = {
  diplome: string;
  school: string;
  schoolLink?: string;
  time: string;
  address: string;
  work: string;
};

const Details = ({
  diplome,
  school,
  schoolLink,
  time,
  address,
  work,
}: DetailsProps) => {
  const ref = useRef(null);
  return (
    <li
      ref={ref}
      className="my-8 first:mt-0 last:mb-0 w-[60%] mx-auto flex flex-col items-center justify-between md:w-[80%]"
    >
      <LiIcon reference={ref} />
      <motion.div
        initial={{ y: 50 }}
        whileInView={{ y: 0 }}
        transition={{ duration: 0.5, type: "spring" }}
      >
        <h3 className="capitalize font-bold text-2xl sm:text-xl xs:text-lg">
          {diplome}&nbsp;
          <a
            href={schoolLink}
            target="_blank"
            className="text-primary dark:text-primaryDark capitalize"
          >
            @{school}
          </a>
        </h3>
        <span className="capitalize font-medium text-dark/75 dark:text-light/75 xs:text-sm">
          {time} | {address}
        </span>
        <p className="font-medium w-full xs:text-sm">{work}</p>
      </motion.div>
    </li>
  );
};

function Education() {
  const ref = useRef(null);
  const { scrollYProgress } = useScroll({
    target: ref,
    offset: ["start end", "center start"],
  });
  return (
    <div className="my-64">
      <h2 className="font-bold text-8xl mb-32 w-full text-center md:text-6xl xs:text-4xl md:mb-16">
        Education
      </h2>

      <div className="w-[75%] mx-auto relative lg:w-[90%] md:w-full" ref={ref}>
        <motion.div
          style={{ scaleY: scrollYProgress }}
          className="absolute left-9 top-0 w-[4px] h-full bg-dark dark:bg-light origin-top 
          md:w-[2px] md:left-[30px] xs:left-[20px]"
        ></motion.div>
        <ul className="w-full flex flex-col items-start justify-between ml-4 xs:ml-2">
          <Details
            school="Efrei Paris"
            schoolLink="https://www.efrei.fr/"
            address="30-32 Av. de la République, VilleJuif"
            diplome="Master degree in Engineering - Software Engineer"
            time="2021 - 2023"
            work="After completing my Bachelor’s degree, I progressed to earn a Master's degree in Engineering with a specialization in Software Engineering. This advanced program was a significant milestone in my educational journey, where I deepened my understanding of web development and cloud technologies. Additionally, it broadened my horizons with valuable insights into machine learning and marketing. This period was instrumental in refining my technical abilities and preparing me for the diverse and evolving challenges in the field of software engineering."
          />
          <Details
            school="Efrei Paris"
            schoolLink="https://www.efrei.fr/"
            address="30-32 Av. de la République, VilleJuif"
            diplome="Bachelor's degree - Computer Science"
            time="2017 - 2021"
            work="My academic journey at EFREI Paris, a renowned general engineering school, laid the foundation of my engineering expertise. Beyond the core engineering curriculum, the school offered me a holistic educational experience. I delved into complementary fields like communication, economics, and marketing, enriching my skill set and providing me with a well-rounded perspective that transcends traditional engineering boundaries."
          />
        </ul>
      </div>
    </div>
  );
}

export default Education;
