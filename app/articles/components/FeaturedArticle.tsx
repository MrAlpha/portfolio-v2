import Image, { StaticImageData } from "next/image";
import Link from "next/link";
import React from "react";

type Props = {
  image: StaticImageData;
  title: string;
  time: string;
  summary: string;
  link?: string;
};

function FeaturedArticle({ image, title, time, summary, link }: Props) {
  return (
    <li className="col-span-2 w-full p-4 bg-light border border-solid border-dark rounded-2xl">
      <Link
        href={link ? link : ""}
        target="_blank"
        className="w-full cursor-pointer overflow-hidden rounded-lg"
      >
        <Image src={image} alt={title} className="w-full h-auto" />
      </Link>
      <Link href={link ? link : ""} target="_blank">
        <h2 className="capitalize text-2xl font-bold my-2">{title}</h2>
      </Link>
      <p className="text-sm mb-2">{summary}</p>
      <span className="text-primary font-semibold">{time}</span>
    </li>
  );
}

export default FeaturedArticle;
