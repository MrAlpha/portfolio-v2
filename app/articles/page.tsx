import React from "react";
import CustomLayout from "../components/CustomLayout";
import AnimatedText from "../components/AnimatedText";
import FeaturedArticle from "./components/FeaturedArticle";
import HocArticleImage from "../../public/images/articles/What is higher order component in React.jpg";

function page() {
  return (
    <>
      <main className="w-full mb-16 flex flex-col items-center justify-center overflow-hidden">
        <CustomLayout className="pt-16">
          <AnimatedText
            text="Words Can Change The World!"
            className="!text-8xl mb-16"
          />
          <ul className="gird grid-cols-4 gap-16">
            <FeaturedArticle
              image={HocArticleImage}
              title="What Is Higher Order Component (Hoc) In React?"
              summary="test"
              time="1st January 2024"
              link="/"
            />
            <FeaturedArticle
              image={HocArticleImage}
              title="What Is Higher Order Component (Hoc) In React?"
              summary="test"
              time="1st January 2024"
              link="/"
            />
          </ul>
        </CustomLayout>
      </main>
    </>
  );
}

export default page;
